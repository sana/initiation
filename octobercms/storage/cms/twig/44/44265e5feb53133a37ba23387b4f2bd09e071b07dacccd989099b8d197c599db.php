<?php

/* C:\xampp\htdocs\octobercms/themes/rainlab-bonjour/partials/header.htm */
class __TwigTemplate_d1104728f748c6521361d923ff27f4dd13327ac2e4da52c3dba5d78b4c04b1ce extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"main-header\">
    <div class=\"container clearfix\">
        <h1 class=\"logo\">Stage Produits</h1>

        <nav class=\"main-nav\">
            <ul>
                <li class=\"";
        // line 7
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "id", array()) == "homepage")) {
            echo " active ";
        }
        echo "\"><a href=\"http://localhost:1234/octobercms\">accueil</a></li>
                <li class=\"";
        // line 8
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "id", array()) == "Produits")) {
            echo " active ";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("produits");
        echo "\">Produits</a></li>
                <li class=\"";
        // line 9
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "id", array()) == "ctgs")) {
            echo " active ";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("categories");
        echo "\">Categories</a></li>
                <li class=\"";
        // line 10
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "id", array()) == "login")) {
            echo " active ";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("login");
        echo "\">
                    ";
        // line 11
        if ( !($context["user"] ?? null)) {
            // line 12
            echo "                        Connexion
                    ";
        } else {
            // line 14
            echo "                        profile
                    ";
        }
        // line 16
        echo "                </a></li>
                ";
        // line 17
        if (($context["user"] ?? null)) {
            // line 18
            echo "                    <li><a data-request=\"onLogout\" data-request-data=\"redirect: '/'\">Déconnexion</a></li>
                ";
        }
        // line 20
        echo "            </ul>
        </nav>
    </div>

</header>";
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\octobercms/themes/rainlab-bonjour/partials/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 20,  76 => 18,  74 => 17,  71 => 16,  67 => 14,  63 => 12,  61 => 11,  53 => 10,  45 => 9,  37 => 8,  31 => 7,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header class=\"main-header\">
    <div class=\"container clearfix\">
        <h1 class=\"logo\">Stage Produits</h1>

        <nav class=\"main-nav\">
            <ul>
                <li class=\"{% if this.page.id == 'homepage' %} active {% endif %}\"><a href=\"http://localhost:1234/octobercms\">accueil</a></li>
                <li class=\"{% if this.page.id == 'Produits' %} active {% endif %}\"><a href=\"{{ 'produits'|page }}\">Produits</a></li>
                <li class=\"{% if this.page.id == 'ctgs' %} active {% endif %}\"><a href=\"{{ 'categories'|page }}\">Categories</a></li>
                <li class=\"{% if this.page.id == 'login' %} active {% endif %}\"><a href=\"{{ 'login'|page }}\">
                    {% if not user  %}
                        Connexion
                    {% else %}
                        profile
                    {% endif %}
                </a></li>
                {% if user %}
                    <li><a data-request=\"onLogout\" data-request-data=\"redirect: '/'\">Déconnexion</a></li>
                {% endif %}
            </ul>
        </nav>
    </div>

</header>", "C:\\xampp\\htdocs\\octobercms/themes/rainlab-bonjour/partials/header.htm", "");
    }
}
