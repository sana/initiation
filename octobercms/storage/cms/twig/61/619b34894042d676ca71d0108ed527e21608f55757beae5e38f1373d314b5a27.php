<?php

/* C:\xampp\htdocs\octobercms/themes/rainlab-bonjour/pages/carte.htm */
class __TwigTemplate_d582477f6d35a6b768ac08d7c795bbdba8d1067c8b6b0aa26562c90963b20ec4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("session"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 2
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", array());
        // line 3
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", array());
        // line 4
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", array());
        // line 5
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", array());
        // line 6
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", array());
        // line 7
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", array());
        // line 8
        echo "
<ul class=\"record-list\">
    ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 11
            echo "    ";
            if ((twig_get_attribute($this->env, $this->source, $context["record"], "user_id", array()) == twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", array()))) {
                // line 12
                echo "    

        <li>
            ";
                // line 16
                echo "            ";
                ob_start();
                // line 17
                echo "                ";
                if (($context["detailsPage"] ?? null)) {
                    // line 18
                    echo "                    <a href=\"";
                    echo $this->extensions['Cms\Twig\Extension']->pageFilter(($context["detailsPage"] ?? null), array(($context["detailsUrlParameter"] ?? null) => twig_get_attribute($this->env, $this->source, $context["record"], ($context["detailsKeyColumn"] ?? null))));
                    echo "\">
                ";
                }
                // line 20
                echo "
                
                ";
                // line 22
                $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "records", array());
                // line 23
                echo "                ";
                $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "displayColumn", array());
                // line 24
                echo "                ";
                $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "noRecordsMessage", array());
                // line 25
                echo "                ";
                $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "detailsPage", array());
                // line 26
                echo "                ";
                $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "detailsKeyColumn", array());
                // line 27
                echo "                ";
                $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList2"] ?? null), "detailsUrlParameter", array());
                // line 28
                echo "
                        <ul class=\"record-list\">
                            ";
                // line 30
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
                $context['_iterated'] = false;
                foreach ($context['_seq'] as $context["_key"] => $context["rcd"]) {
                    // line 31
                    echo "                                <li>
                                    ";
                    // line 33
                    echo "                                    ";
                    ob_start();
                    // line 34
                    echo "                                        ";
                    if (($context["detailsPage"] ?? null)) {
                        // line 35
                        echo "                                            <a href=\"";
                        echo $this->extensions['Cms\Twig\Extension']->pageFilter(($context["detailsPage"] ?? null), array(($context["detailsUrlParameter"] ?? null) => twig_get_attribute($this->env, $this->source, $context["record"], ($context["detailsKeyColumn"] ?? null))));
                        echo "\">
                                        ";
                    }
                    // line 37
                    echo "                                        ";
                    if ((twig_get_attribute($this->env, $this->source, $context["rcd"], "id", array()) == twig_get_attribute($this->env, $this->source, $context["record"], "produit_id", array()))) {
                        // line 38
                        echo "                                           
                                           ";
                        // line 39
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["rcd"], "nom", array()), "html", null, true);
                        echo "
                                           <img src=\"";
                        // line 40
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["rcd"], "IMAGE", array()), "thumb", array(0 => 200, 1 => ($context["auto"] ?? null)), "method"), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["rcd"], "prix", array()), "html", null, true);
                        echo " dt
                                           
                                           
                                        ";
                    }
                    // line 44
                    echo "                                    

                                        ";
                    // line 46
                    if (($context["detailsPage"] ?? null)) {
                        // line 47
                        echo "                                            </a>
                                        ";
                    }
                    // line 49
                    echo "                                    ";
                    echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
                    // line 50
                    echo "                                </li>
                            ";
                    $context['_iterated'] = true;
                }
                if (!$context['_iterated']) {
                    // line 52
                    echo "                                <li class=\"no-data\">";
                    echo twig_escape_filter($this->env, ($context["noRecordsMessage"] ?? null), "html", null, true);
                    echo "</li>
                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rcd'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 54
                echo "                        </ul>

                        ";
                // line 56
                if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", array()) > 1)) {
                    // line 57
                    echo "                            <ul class=\"pagination\">
                                ";
                    // line 58
                    if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", array()) > 1)) {
                        // line 59
                        echo "                                    <li><a href=\"";
                        echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "baseFileName", array()), array(($context["pageParam"] ?? null) => (twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", array()) - 1)));
                        echo "\">&larr; Prev</a></li>
                                ";
                    }
                    // line 61
                    echo "
                                ";
                    // line 62
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", array())));
                    foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                        // line 63
                        echo "                                    <li class=\"";
                        echo (((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", array()) == $context["page"])) ? ("active") : (null));
                        echo "\">
                                        <a href=\"";
                        // line 64
                        echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "baseFileName", array()), array(($context["pageParam"] ?? null) => $context["page"]));
                        echo "\">";
                        echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                        echo "</a>
                                    </li>
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 67
                    echo "
                                ";
                    // line 68
                    if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", array()) > twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", array()))) {
                        // line 69
                        echo "                                      <li><a href=\"";
                        echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "baseFileName", array()), array(($context["pageParam"] ?? null) => (twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", array()) + 1)));
                        echo "\">Next &rarr;</a></li>
                                ";
                    }
                    // line 71
                    echo "                            </ul>
                        ";
                }
                // line 73
                echo "                
                
                

                ";
                // line 77
                if (($context["detailsPage"] ?? null)) {
                    // line 78
                    echo "                    </a>
                ";
                }
                // line 80
                echo "            ";
                echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
                // line 81
                echo "        </li>
    
    ";
            }
            // line 84
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 85
        echo "    
</ul>

";
        // line 88
        if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", array()) > 1)) {
            // line 89
            echo "    <ul class=\"pagination\">
        ";
            // line 90
            if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", array()) > 1)) {
                // line 91
                echo "            <li><a href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "baseFileName", array()), array(($context["pageParam"] ?? null) => (twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", array()) - 1)));
                echo "\">&larr; Prev</a></li>
        ";
            }
            // line 93
            echo "
        ";
            // line 94
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", array())));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 95
                echo "            <li class=\"";
                echo (((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", array()) == $context["page"])) ? ("active") : (null));
                echo "\">
                <a href=\"";
                // line 96
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "baseFileName", array()), array(($context["pageParam"] ?? null) => $context["page"]));
                echo "\">";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "</a>
            </li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 99
            echo "
        ";
            // line 100
            if ((twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "lastPage", array()) > twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", array()))) {
                // line 101
                echo "            <li><a href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", array()), "baseFileName", array()), array(($context["pageParam"] ?? null) => (twig_get_attribute($this->env, $this->source, ($context["records"] ?? null), "currentPage", array()) + 1)));
                echo "\">Next &rarr;</a></li>
        ";
            }
            // line 103
            echo "    </ul>
";
        }
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\octobercms/themes/rainlab-bonjour/pages/carte.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  292 => 103,  286 => 101,  284 => 100,  281 => 99,  270 => 96,  265 => 95,  261 => 94,  258 => 93,  252 => 91,  250 => 90,  247 => 89,  245 => 88,  240 => 85,  234 => 84,  229 => 81,  226 => 80,  222 => 78,  220 => 77,  214 => 73,  210 => 71,  204 => 69,  202 => 68,  199 => 67,  188 => 64,  183 => 63,  179 => 62,  176 => 61,  170 => 59,  168 => 58,  165 => 57,  163 => 56,  159 => 54,  150 => 52,  144 => 50,  141 => 49,  137 => 47,  135 => 46,  131 => 44,  122 => 40,  118 => 39,  115 => 38,  112 => 37,  106 => 35,  103 => 34,  100 => 33,  97 => 31,  92 => 30,  88 => 28,  85 => 27,  82 => 26,  79 => 25,  76 => 24,  73 => 23,  71 => 22,  67 => 20,  61 => 18,  58 => 17,  55 => 16,  50 => 12,  47 => 11,  43 => 10,  39 => 8,  37 => 7,  35 => 6,  33 => 5,  31 => 4,  29 => 3,  27 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% component 'session' %}
{% set records = builderList.records %}
{% set displayColumn = builderList.displayColumn %}
{% set noRecordsMessage = builderList.noRecordsMessage %}
{% set detailsPage = builderList.detailsPage %}
{% set detailsKeyColumn = builderList.detailsKeyColumn %}
{% set detailsUrlParameter = builderList.detailsUrlParameter %}

<ul class=\"record-list\">
    {% for record in records %}
    {% if record.user_id==user.id %}
    

        <li>
            {# Use spaceless tag to remove spaces inside the A tag. #}
            {% spaceless %}
                {% if detailsPage %}
                    <a href=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(record, detailsKeyColumn) }) }}\">
                {% endif %}

                
                {% set records = builderList2.records %}
                {% set displayColumn = builderList2.displayColumn %}
                {% set noRecordsMessage = builderList2.noRecordsMessage %}
                {% set detailsPage = builderList2.detailsPage %}
                {% set detailsKeyColumn = builderList2.detailsKeyColumn %}
                {% set detailsUrlParameter = builderList2.detailsUrlParameter %}

                        <ul class=\"record-list\">
                            {% for rcd in records %}
                                <li>
                                    {# Use spaceless tag to remove spaces inside the A tag. #}
                                    {% spaceless %}
                                        {% if detailsPage %}
                                            <a href=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(record, detailsKeyColumn) }) }}\">
                                        {% endif %}
                                        {% if rcd.id==record.produit_id %}
                                           
                                           {{rcd.nom}}
                                           <img src=\"{{ rcd.IMAGE.thumb(200,auto) }}\">{{ rcd.prix }} dt
                                           
                                           
                                        {% endif %}
                                    

                                        {% if detailsPage %}
                                            </a>
                                        {% endif %}
                                    {% endspaceless %}
                                </li>
                            {% else %}
                                <li class=\"no-data\">{{ noRecordsMessage }}</li>
                            {% endfor %}
                        </ul>

                        {% if records.lastPage > 1 %}
                            <ul class=\"pagination\">
                                {% if records.currentPage > 1 %}
                                    <li><a href=\"{{ this.page.baseFileName|page({ (pageParam): (records.currentPage-1) }) }}\">&larr; Prev</a></li>
                                {% endif %}

                                {% for page in 1..records.lastPage %}
                                    <li class=\"{{ records.currentPage == page ? 'active' : null }}\">
                                        <a href=\"{{ this.page.baseFileName|page({ (pageParam): page }) }}\">{{ page }}</a>
                                    </li>
                                {% endfor %}

                                {% if records.lastPage > records.currentPage %}
                                      <li><a href=\"{{ this.page.baseFileName|page({ (pageParam): (records.currentPage+1) }) }}\">Next &rarr;</a></li>
                                {% endif %}
                            </ul>
                        {% endif %}
                
                
                

                {% if detailsPage %}
                    </a>
                {% endif %}
            {% endspaceless %}
        </li>
    
    {% endif %}
    {% endfor %}
    
</ul>

{% if records.lastPage > 1 %}
    <ul class=\"pagination\">
        {% if records.currentPage > 1 %}
            <li><a href=\"{{ this.page.baseFileName|page({ (pageParam): (records.currentPage-1) }) }}\">&larr; Prev</a></li>
        {% endif %}

        {% for page in 1..records.lastPage %}
            <li class=\"{{ records.currentPage == page ? 'active' : null }}\">
                <a href=\"{{ this.page.baseFileName|page({ (pageParam): page }) }}\">{{ page }}</a>
            </li>
        {% endfor %}

        {% if records.lastPage > records.currentPage %}
            <li><a href=\"{{ this.page.baseFileName|page({ (pageParam): (records.currentPage+1) }) }}\">Next &rarr;</a></li>
        {% endif %}
    </ul>
{% endif %}", "C:\\xampp\\htdocs\\octobercms/themes/rainlab-bonjour/pages/carte.htm", "");
    }
}
