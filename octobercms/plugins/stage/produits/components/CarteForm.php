<?php namespace Stage\Produits\Components;
 use Cms\Classes\ComponentBase;
 use stage\produit\models\Carte;

 class CarteForm extends ComponentBase
 {
	public function componentDetails(){
		return [ 
			'name' => 'Carte Form',
			'description' => 'Simple carte form'
		];
	}
	public function onSave(){
		$id1=user.id;$id2=record.id;
		$carte = new Carte();
		$carte->user_id = $id1;
		$carte->produit_id = $id2;
    	$carte->save();
	}
	
}
