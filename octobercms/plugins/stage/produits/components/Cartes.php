<?php namespace stage\produits\components;

use cms\classes\componentBase;
use stage\produits\models\carte;


class Cartes extends ComponentBase
{
	
	public function componentDetails()
	{
		return [ 'name' => 'carte list', 'description' => 'carte'];
	}

	public function onRan() 
	{
		$this->cartes = $this->loadCartes();
	}
	protected function loadCartes() 
		{return carte::all();}
	public $cartes;
}
