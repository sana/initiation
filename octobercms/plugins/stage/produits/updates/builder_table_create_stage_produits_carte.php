<?php namespace Stage\Produits\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateStageProduitsCarte extends Migration
{
    public function up()
    {
        Schema::create('stage_produits_carte', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('user_id')->unsigned();
            $table->integer('produit_id')->unsigned();
            $table->primary(['user_id','produit_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('stage_produits_carte');
    }
}
