<?php namespace Stage\Produits\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateStageProduits2 extends Migration
{
    public function up()
    {
        Schema::table('stage_produits_', function($table)
        {
            $table->increments('id')->nullable(false)->unsigned()->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('stage_produits_', function($table)
        {
            $table->string('id', 191)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
