<?php namespace Stage\Produits\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateStageProduitsAppartenirCtgs extends Migration
{
    public function up()
    {
        Schema::rename('stage_produits_appartenir_categorie', 'stage_produits_appartenir_ctgs');
    }
    
    public function down()
    {
        Schema::rename('stage_produits_appartenir_ctgs', 'stage_produits_appartenir_categorie');
    }
}
