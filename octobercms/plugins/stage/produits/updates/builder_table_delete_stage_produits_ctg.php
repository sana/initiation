<?php namespace Stage\Produits\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteStageProduitsCtg extends Migration
{
    public function up()
    {
        Schema::dropIfExists('stage_produits_ctg');
    }
    
    public function down()
    {
        Schema::create('stage_produits_ctg', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('categorie', 191);
            $table->string('slug', 191);
        });
    }
}
