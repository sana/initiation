<?php namespace Stage\Produits\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateStageProduitsCtg extends Migration
{
    public function up()
    {
        Schema::rename('stage_produits_categorie', 'stage_produits_ctg');
        Schema::table('stage_produits_ctg', function($table)
        {
            $table->renameColumn('categorie', 'ctg');
        });
    }
    
    public function down()
    {
        Schema::rename('stage_produits_ctg', 'stage_produits_categorie');
        Schema::table('stage_produits_categorie', function($table)
        {
            $table->renameColumn('ctg', 'categorie');
        });
    }
}
