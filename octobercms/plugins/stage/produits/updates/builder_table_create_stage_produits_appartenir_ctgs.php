<?php namespace Stage\Produits\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateStageProduitsAppartenirCtgs extends Migration
{
    public function up()
    {
        Schema::create('stage_produits_appartenir_ctgs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('produit_id');
            $table->integer('ctg_id');
            $table->primary(['produit_id','ctg_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('stage_produits_appartenir_ctgs');
    }
}
