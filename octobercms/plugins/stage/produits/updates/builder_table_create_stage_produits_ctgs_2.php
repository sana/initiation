<?php namespace Stage\Produits\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateStageProduitsCtgs2 extends Migration
{
    public function up()
    {
        Schema::create('stage_produits_ctgs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('ctg');
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('stage_produits_ctgs');
    }
}
