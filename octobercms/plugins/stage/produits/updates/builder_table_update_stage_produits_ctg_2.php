<?php namespace Stage\Produits\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateStageProduitsCtg2 extends Migration
{
    public function up()
    {
        Schema::rename('stage_produits_categorie', 'stage_produits_ctg');
    }
    
    public function down()
    {
        Schema::rename('stage_produits_ctg', 'stage_produits_categorie');
    }
}
