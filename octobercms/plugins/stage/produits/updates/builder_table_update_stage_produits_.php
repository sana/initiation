<?php namespace Stage\Produits\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateStageProduits extends Migration
{
    public function up()
    {
        Schema::table('stage_produits_', function($table)
        {
            $table->dropPrimary(['sku']);
            $table->renameColumn('sku', 'id');
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::table('stage_produits_', function($table)
        {
            $table->dropPrimary(['id']);
            $table->renameColumn('id', 'sku');
            $table->primary(['sku']);
        });
    }
}
