<?php namespace Stage\Produits\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateStageProduits3 extends Migration
{
    public function up()
    {
        Schema::table('stage_produits_', function($table)
        {
            $table->string('sku');
        });
    }
    
    public function down()
    {
        Schema::table('stage_produits_', function($table)
        {
            $table->dropColumn('sku');
        });
    }
}
