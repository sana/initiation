<?php namespace Stage\Produits;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return ['stage\produits\components\cartes' => 'Cartes' ,
    	        'stage\produits\components\carteform' => 'CarteForm'
    	];
    }

    public function registerSettings()
    {
    }
}
